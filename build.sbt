name := "feedle-proxy"

version := "1.0"

mainClass := Some("com.feedle.proxy.Launcher")

libraryDependencies ++= Seq(
  "io.netty"%"netty-all"%"4.0.19.Final",
  "com.jcraft" % "jzlib" % "1.1.3"
)