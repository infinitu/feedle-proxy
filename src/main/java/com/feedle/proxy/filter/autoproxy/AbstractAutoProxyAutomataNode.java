package com.feedle.proxy.filter.autoproxy;

import io.netty.buffer.ByteBuf;

import java.util.Arrays;

/**
 * Created by infinitu on 2014. 6. 16..
 */
public abstract class AbstractAutoProxyAutomataNode {
    protected AbstractAutoProxyAutomataNode[] nextAutomata = new AbstractAutoProxyAutomataNode[256];

    protected void fillNotFound(){
        Arrays.fill(nextAutomata, NotFoundNode.getInstace());
    }

    public AbstractAutoProxyAutomataNode findPattern(ByteBuf buffer, int startIdx){
        if(!buffer.isReadable()){
            buffer.readerIndex(startIdx);
            return this;
        }
        byte readByte = buffer.readByte();
        if(readByte<0) return NotFoundNode.getInstace().findPattern(buffer,startIdx);
        return nextAutomata[readByte].findPattern(buffer,startIdx);
    }

    protected void merge(AbstractAutoProxyAutomataNode newNode){
        for(int idx=0;idx<256;idx++){
            if (NotFoundNode.class.isInstance(newNode.nextAutomata[idx]))
                continue;

            if(idx==47)
                continue;

            if (!NotFoundNode.class.isInstance(nextAutomata[idx])){
                nextAutomata[idx] = ((DefaultNode)nextAutomata[idx]).clone();
                nextAutomata[idx].merge(newNode.nextAutomata[idx]);
                continue;
            }

            nextAutomata[idx] = newNode.nextAutomata[idx];
        }
    }
}
