package com.feedle.proxy.filter.autoproxy;

/**
 * Created by infinitu on 2014. 6. 16..
 */
public class SlashNode extends DefaultNode {

    protected DefaultNode dot;

    protected SlashNode(SlashNode previousSlash) {
        super('/',previousSlash);
        this.nextAutomata['/'] = this;

        DefaultNode dotdot = new DefaultNode('.',this);
        dot = new DefaultNode('.',this);
        dot.nextAutomata['.']=dotdot;
        dot.nextAutomata['/']=this;
        dotdot.nextAutomata['/'] = previousSlash;
        this.previousSlash = this;
    }
}
