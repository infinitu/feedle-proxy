package com.feedle.proxy.filter.autoproxy;

import java.util.Arrays;

/**
 * Created by infinitu on 2014. 6. 16..
 */
public class DefaultNode extends AbstractAutoProxyAutomataNode {

    private String str;
    protected SlashNode previousSlash;

    protected DefaultNode(char c,SlashNode previousSlash){
        this(new String(new char[]{c}), previousSlash);
        this.fillNotFound();
    }

    protected DefaultNode(String c,SlashNode previousSlash){
        this.str = c;
        this.previousSlash=previousSlash;
    }

    protected DefaultNode add(char b){
        if (NotFoundNode.class.isInstance(nextAutomata[b])) {
            if(b=='/')
                nextAutomata[b] = new SlashNode(previousSlash);
            else
                nextAutomata[b] = new DefaultNode(b,previousSlash);
        }
        return (DefaultNode) nextAutomata[b];
    }

    @Override
    public String toString() {
        return str;
    }

    @Override
    public DefaultNode clone(){
        DefaultNode newins = new DefaultNode(str,previousSlash);
        System.arraycopy(nextAutomata,0,newins.nextAutomata,0,256);
        return newins;
    }
}
