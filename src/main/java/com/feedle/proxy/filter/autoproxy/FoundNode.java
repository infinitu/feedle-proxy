package com.feedle.proxy.filter.autoproxy;

import io.netty.buffer.ByteBuf;

/**
 * Created by infinitu on 2014. 6. 16..
 */
public class FoundNode extends DefaultNode{

    public byte[] foundUrl;

    protected FoundNode(String url){
        super("found " + url,null);
        this.foundUrl = (url).getBytes();
    }

    @Override
    public AbstractAutoProxyAutomataNode findPattern(ByteBuf buffer, int startIdx) {
        return this;
    }
}
