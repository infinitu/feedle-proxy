package com.feedle.proxy.filter.autoproxy;

import io.netty.buffer.ByteBuf;

/**
 * Created by infinitu on 2014. 6. 16..
 */
public class NotFoundNode extends DefaultNode{

    private static NotFoundNode instance = null;
    public static NotFoundNode getInstace(){
        if(instance == null)
            instance = new NotFoundNode();
        return instance;
    }

    private NotFoundNode(){
        super("notFound!",null);
    }

    @Override
    public AbstractAutoProxyAutomataNode findPattern(ByteBuf buffer, int startIdx) {
        buffer.readerIndex(startIdx);
        return this;
    }
}
