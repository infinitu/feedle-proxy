package com.feedle.proxy.filter.autoproxy;

/**
 * Created by infinitu on 2014. 6. 16..
 */
public class HeadNode extends AbstractAutoProxyAutomataNode {

    protected HeadNode(AbstractAutoProxyAutomataNode startNode){
        fillNotFound();
        nextAutomata['\"'] = startNode;
        nextAutomata['\''] = startNode;
    }
}
