package com.feedle.proxy.filter.autoproxy;

/**
 * Created by infinitu on 2014. 6. 16..
 */
public class ProxyAutomataFactory {

    public static class AutomataHolder {
        private String host;
        private DefaultNode startNode;
        private DefaultNode fullUrlNode;
        private DefaultNode schemeSlash;
        private DefaultNode hostTail;
        private DefaultNode rootSlash;
        private DefaultNode relPathSlash;
        protected AutomataHolder(String host){
            this.host=host;
            startNode = new DefaultNode("<start_node>",null);
            startNode.fillNotFound();
            DefaultNode tail;
            tail = startNode.add('h');

            fullUrlNode = tail;
            tail = tail.add('t').add('t').add('p').add(':').add('/');

            SlashNode doubleSlash = new SlashNode(null);
            doubleSlash.previousSlash=doubleSlash;
            tail.nextAutomata['/'] = doubleSlash;

            schemeSlash = doubleSlash;

            byte[] arr = host.getBytes();
            for (byte anArr : arr) {
                tail = tail.add((char) anArr);
            }
            hostTail=tail;
            tail = tail.add('/');
            rootSlash = tail;

            relPathSlash = new DefaultNode('/',null);
        }

        public void addPath(String path,String destFullUrl){
            DefaultNode tail=hostTail;

            byte[] bytes = path.getBytes();
            for(byte b : bytes){
                tail=tail.add((char) b);
            }

            FoundNode found = new FoundNode(destFullUrl);

            tail.nextAutomata['\"'] = found;
            tail.nextAutomata['\''] = found;
            tail.nextAutomata['?'] = found;
        }


        /*
         *  NOTE Test Cases
         *      -  http://naver.com/a.jpg
         *      -  /a.jpg
         *      -  //naver.com/a.jpg
         *      -  /././a.jpg
         *      -  ../.././a.jpg
         *      -  http.jpg
         */
        public AbstractAutoProxyAutomataNode createAutomata(String newPath){
            DefaultNode tail = hostTail;
            byte[] arr = host.getBytes();
            for (byte anArr : arr) {
                tail = tail.add((char) anArr);
            }

            DefaultNode start = startNode.clone();
            DefaultNode root = rootSlash.clone();
            root.nextAutomata['/'] = schemeSlash;
            start.nextAutomata['/']=root;
            start.merge(tail.previousSlash);

            return new HeadNode(start);
        }

    }

    public static AutomataHolder createHolder(String host){
        return new AutomataHolder(host);
    }

}
