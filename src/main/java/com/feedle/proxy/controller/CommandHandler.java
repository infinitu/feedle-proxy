package com.feedle.proxy.controller;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

/**
 * Created by infinitu on 2014. 6. 11..
 */
public class CommandHandler extends ChannelInboundHandlerAdapter {
    private CommandServer server=null;

    protected CommandHandler(CommandServer server){
        this.server=server;
    }

    final Object lock = new Object();
    int command = -1;
    int length =-1;
    byte[] content;
    int idx = 0;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        byte[] bytes = (byte[]) msg;
        int lastLength;
        synchronized (lock) {
            if (command < 0) {
                command = bytes[0] & 0xff;
                length = ((bytes[1] & 0xff) << 8) | (bytes[2] & 0xff);
                content = new byte[length];
                lastLength = bytes.length - 3;
                if (lastLength < length) {
                    System.arraycopy(bytes, 3, content, 0, lastLength);
                    idx = lastLength;
                } else {
                    System.arraycopy(bytes, 3, content, 0, length);
                    server.command(command, content);
                    command = -1;
                    lastBytes(ctx, bytes,length+3);
                }
            } else {
                lastLength = bytes.length;
                if (lastLength < length - idx) {
                    System.arraycopy(bytes, 0, content, idx, lastLength);
                    idx += lastLength;
                } else {
                    System.arraycopy(bytes, 0, content, idx, length - idx);
                    server.command(command, content);
                    command = -1;
                    lastBytes(ctx, bytes,length - idx);
                }
            }
        }
        ReferenceCountUtil.release(msg);
    }

    public void lastBytes(ChannelHandlerContext ctx, byte[] msg, int usedByte) throws Exception {
        int length = msg.length-usedByte;
        if(length<1) return;
        byte[] newByte = new byte[length];
        System.arraycopy(msg,usedByte,newByte,0,length);
        channelRead(ctx, ReferenceCountUtil.retain(newByte));

    }
}
