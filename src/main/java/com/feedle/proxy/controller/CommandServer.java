package com.feedle.proxy.controller;

import com.feedle.proxy.ProxyServer;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.util.concurrent.TimeUnit;


/**
 * Created by infinitu on 2014. 6. 11..
 */
public class CommandServer {

    public static final byte[] COMMAND_VERSION = {0x00,0x01};
    private static final String FEEDLE_ENGINE_HOST = "www.feedle.kr";

    private EventLoopGroup workerGroup = new NioEventLoopGroup();
    private Channel channel = null;
    private ProxyServer proxy=null;
    private int port = 0;
    Bootstrap boot = new Bootstrap();

    private void tryReConnectAt10SecLater(){
        System.out.println("Retry to Connect");
        workerGroup.schedule(new Runnable() {
            @Override
            public void run() {
                CommandServer.this.run();
            }
        },10, TimeUnit.SECONDS);
    }

    public CommandServer(ProxyServer proxy,int port){
        this.proxy = proxy;
        this.port = port;
        boot.group(workerGroup);
        boot.channel(NioSocketChannel.class);
        boot.option(ChannelOption.SO_KEEPALIVE,true);
        boot.handler(new ChannelInitializer<SocketChannel>(){
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                channel=ch;
                ch.pipeline()
                        .addLast(new LoggingHandler(LogLevel.INFO))
                        .addLast(new ByteArrayDecoder())
                        .addLast(new ByteArrayEncoder())
                        .addLast(new CommandHandler(CommandServer.this));
                ch.pipeline().addLast(new RebootHandler());

            }
        });
    }

    public void run(){
        boot.connect(FEEDLE_ENGINE_HOST,port).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if(future.isSuccess())
                    System.out.println("Proxy Command Server Connection Success");
//                else
//                    tryReConnectAt10SecLater();

            }
        });
    }

    public void command(int command, byte[] content) {
        System.out.printf("command:%x  content:%s", command, new String(content));
        switch (command){
            case 0x00: //    Heartbeat command
                sendHeartbeat();
                break;
            case 0x11: //    Proxy Configuration Deployment
                proxy.deployConfig(this,new String(content));
                break;
            case 0x12: //    Stop Proxy Server
                proxy.stopProxing(this,new String(content));
                break;
            case 0x13: //    Stop All Proxy
                proxy.stopAllProxing(this);
                break;
            case 0x14: //    Get All Sarted Proxy ( “\n”로 구분])
                proxy.getAllServer(this);
                break;
            case 0x21: //    Get Proxy Traffic Count
                proxy.getProxyTrafficCount(this,new String(content),false);
                break;
            case 0x22: //    Get Proxy Traffic Count And Reset Counter
                proxy.getProxyTrafficCount(this,new String(content),true);
                break;
            case 0x23: //    Reset Traffic Counter
                proxy.resetProxyTrafficCount(this,new String(content));
                break;
            case 0x24: //    Get All Proxy Traffic Count
                proxy.getAllProxyTrafficCount(this,false);
                break;
            case 0x25: //    Get All Proxy Traffic Count And Reset Counter
                proxy.getAllProxyTrafficCount(this,true);
                break;
            case 0x26: //    Reset All Traffic Counter
                proxy.resetAllProxyTrafficCount(this);
                break;
        }
    }

    public void sendHeartbeat() {
        channel.write(new byte[]{0x00,0x00,0x02,COMMAND_VERSION[0],COMMAND_VERSION[1]});
    }

    public void sendOK(String str) {
        sendMsg((byte) 0x10,str);
    }

    public void sendError(String str) {
        sendMsg((byte) 0x20,str);
    }

    public void sendNotReady() {
        sendMsg((byte) 0x30,"");
    }

    private void sendMsg(byte code, String str){
        if(str==null) str="";
        byte[] content = str.getBytes();
        byte[] msg = new byte[3+content.length];
        msg[0]= code;
        msg[1]= (byte) (content.length>>8);
        msg[2]= (byte) (content.length&0xff);
        System.arraycopy(content,0,msg,3,content.length);
        channel.writeAndFlush(msg);
    }

    private class RebootHandler extends ChannelInboundHandlerAdapter{
        @Override
        public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
            tryReConnectAt10SecLater();
        }
    }
}
