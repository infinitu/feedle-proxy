package com.feedle.proxy

import com.feedle.proxy.controller.CommandServer
import com.feedle.proxy.filter.HttpCompressor
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.channel.{ChannelInitializer, ChannelOption}
import io.netty.handler.codec.http.{HttpContentDecompressor, HttpRequestDecoder, HttpResponseEncoder}

import scala.collection.mutable
import scala.xml.XML

/**
 * Created by infinitu on 2014. 5. 29..
 */
class ProxyServer(val port:Int) {
  val clientBossGroup = new NioEventLoopGroup()
  val clientWorkerGroup = new NioEventLoopGroup()

  val originConnectionPoolMap = new mutable.HashMap[String,OriginConnectionPool]
                                  with mutable.SynchronizedMap[String,OriginConnectionPool]

  if(port==8088){
    val config =XML.loadFile("./src/main/resources/navertest.xml")
    val naver = new OriginConnectionPool(config)
    val host = (config \ "@host").toString()
    originConnectionPoolMap+=host->naver

    val config2 =XML.loadFile("./src/main/resources/hyundai.xml")
    val hyun = new OriginConnectionPool(config2)
    val host2 = (config2 \ "@host").toString()
    originConnectionPoolMap+=host2->hyun
  }
  if(port==443)
  {
    val config =XML.loadFile("./src/main/resources/hyundai_https.xml")
    val hyun = new OriginConnectionPool(config)
    val host = (config \ "@host").toString()
    originConnectionPoolMap+=host->hyun
  }
  val httpCompressHandler = new HttpCompressor()

  def run(){

      try
      {
        val bootstrap = new ServerBootstrap()
        bootstrap.group(clientBossGroup,clientWorkerGroup)
        bootstrap.channel(classOf[NioServerSocketChannel])
        bootstrap.childHandler(new ChannelInitializer[SocketChannel] {
          override def initChannel(ch: SocketChannel){ch.pipeline()
            .addLast(new HttpRequestDecoder())
            .addLast(new HttpResponseEncoder())
            //.addLast("compressor",httpCompressHandler())
            .addLast("Cli-Con-Handle",new ClientConnectionHandler(ProxyServer.this))
            //.addLast(new LoggingHandler(LogLevel.INFO))
          }
        })
        bootstrap.option[java.lang.Integer](ChannelOption.SO_BACKLOG,128)
        bootstrap.childOption[java.lang.Boolean](ChannelOption.SO_KEEPALIVE,true)

        bootstrap.bind(port)//.sync().channel().closeFuture().sync()
      }
      finally {
//        clientWorkerGroup.shutdownGracefully()
//        clientBossGroup.shutdownGracefully()
      }

  }

  private val reQ = new mutable.SynchronizedQueue[()=>Unit]()
  private def reqEnQ(fn:()=>Unit){
    if(reQ.isEmpty){
      reQ enqueue fn
      fn()
    }
    else reQ enqueue fn

  }
  private def reqDeQ(){
    reQ.dequeue()
    if(reQ.nonEmpty) reQ.front.apply()
  }

  def jsonMsg(msg:String) = """{"message":"%s"}""".format(msg)

  def deployConfig(commandServer:CommandServer,xmlStr:String)=reqEnQ {()=>
    val config =XML.loadString(xmlStr)
    val host = (config \ "@host").toString()
    val poolOpt=originConnectionPoolMap.get(host)
    try{
      val newPool = poolOpt match{
        case Some(pool)=>
          originConnectionPoolMap -=host
          new OriginConnectionPool(config,
            pool.bootstrap,
            pool.originWorkerGroup,
            pool.originTrafficCounter,
            pool.clientTrafficCounter,
            pool.originTrafficHandler,
            pool.clientTrafficHandler,
            pool.IdleChannels)
        case None=>
          new OriginConnectionPool(config)
      }
      originConnectionPoolMap += host->newPool
      val msg = if(poolOpt.isEmpty) "New Proxy Started" else "Proxy Restarted"
      commandServer sendOK jsonMsg(msg)
    } catch{
      case ex: Throwable =>
        ex.printStackTrace()
        commandServer.sendError(ex.toString)
    }
    reqDeQ()
  }

  def stopProxing(commandServer:CommandServer,str:String){
    val poolOpt=originConnectionPoolMap.get(str)
    poolOpt match {
      case Some(pool) =>
        originConnectionPoolMap -= str
        pool.originWorkerGroup.shutdownGracefully()
        commandServer.sendOK(jsonMsg("Successfully Stopped"))
      case None =>
        commandServer.sendOK(jsonMsg("Not Exist"))
    }
    reqDeQ()
  }

  def stopAllProxing(commandServer:CommandServer){
    originConnectionPoolMap.foreach(_._2.originWorkerGroup.shutdownGracefully())
    originConnectionPoolMap.clear()
    commandServer.sendOK(jsonMsg("Successfully Stopped"))
  }

  def getAllServer(commandServer:CommandServer){
    val builder = mutable.StringBuilder.newBuilder
    builder.append('[')
    originConnectionPoolMap.foreach(x=>builder.append("\""+x._1+"\","))
    builder.deleteCharAt(builder.size-1)
    builder.append("]")
    commandServer.sendOK(builder.mkString)
  }

  def trafficCountStr(cliR:Long,cliW:Long,proR:Long,proW:Long)={
    def trafficPart(r:Long,w:Long)={
      """{"read"=%d,"write"=%d,"amount"=%d}""".format(r,w,r+w)
    }
    """{"client"=%s,"origin"=%s,"amount"=%d}"""
      .format(trafficPart(cliR,cliW),trafficPart(proR,proW),cliR+cliW+proR+proW)
  }

  def getProxyTrafficCount(commandServer:CommandServer,str:String,reset:Boolean){
    val poolOpt=originConnectionPoolMap.get(str)
    poolOpt match {
      case Some(pool) =>
        val cliR = pool.clientTrafficCounter.currentReadBytes()
        val cliW = pool.clientTrafficCounter.currentWrittenBytes()
        val proR = pool.originTrafficCounter.currentReadBytes()
        val proW = pool.originTrafficCounter.currentWrittenBytes()
        if(reset) {
          pool.clientTrafficCounter.resetCumulativeTime()
          pool.originTrafficCounter.resetCumulativeTime()
        }
        commandServer.sendOK(trafficCountStr(cliR,cliW,proR,proW))
      case None =>
        commandServer.sendError("Not Exist : "+str)
    }
  }

  def resetProxyTrafficCount(commandServer:CommandServer,str:String){
    val poolOpt=originConnectionPoolMap.get(str)
    poolOpt match {
      case Some(pool) =>
        val cliR = pool.clientTrafficCounter.currentReadBytes()
        val cliW = pool.clientTrafficCounter.currentWrittenBytes()
        val proR = pool.originTrafficCounter.currentReadBytes()
        val proW = pool.originTrafficCounter.currentWrittenBytes()
        pool.clientTrafficCounter.resetCumulativeTime()
        pool.originTrafficCounter.resetCumulativeTime()
        commandServer.sendOK(jsonMsg("Reset Complete, All %dBytes Reset".format(cliR+cliW+proR+proW)))
      case None =>
        commandServer.sendError("Not Exist : "+str)
    }
  }

  def getAllProxyTrafficCount(commandServer:CommandServer,reset:Boolean){
    val builder = mutable.StringBuilder.newBuilder
    builder.append('{')
    originConnectionPoolMap.foreach{x=>
      builder.append('\"')
        .append(x._1)
        .append("\":")
      val cliR = x._2.clientTrafficCounter.currentReadBytes()
      val cliW = x._2.clientTrafficCounter.currentWrittenBytes()
      val proR = x._2.originTrafficCounter.currentReadBytes()
      val proW = x._2.originTrafficCounter.currentWrittenBytes()
      if(reset) {
        x._2.clientTrafficCounter.resetCumulativeTime()
        x._2.originTrafficCounter.resetCumulativeTime()
      }
      builder.append(trafficCountStr(cliR,cliW,proR,proW))
        .append(',')

    }
    builder.deleteCharAt(builder.size-1)
    builder.append("}")
    commandServer.sendOK(builder.mkString)
  }

  def resetAllProxyTrafficCount(commandServer:CommandServer){
    var cnt:Long=0
    originConnectionPoolMap.foreach{x=>
      cnt += x._2.clientTrafficCounter.currentReadBytes()
      cnt += x._2.clientTrafficCounter.currentWrittenBytes()
      cnt += x._2.originTrafficCounter.currentReadBytes()
      cnt += x._2.originTrafficCounter.currentWrittenBytes()
      x._2.clientTrafficCounter.resetCumulativeTime()
      x._2.originTrafficCounter.resetCumulativeTime()
    }
    commandServer.sendOK(jsonMsg("Reset Complete, All %dBytes Reset".format(cnt)))
  }
}
