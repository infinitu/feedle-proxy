package com.feedle.proxy

import com.feedle.proxy.filter._
import io.netty.bootstrap.Bootstrap
import io.netty.channel._
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioSocketChannel
import io.netty.handler.codec.http.{HttpContentDecompressor, HttpRequestEncoder, HttpResponseDecoder}
import io.netty.handler.logging.{LogLevel, LoggingHandler}
import io.netty.handler.traffic.{GlobalTrafficShapingHandler, TrafficCounter}

import scala.collection.mutable
import scala.xml.Elem

/**
 * Created by infinitu on 2014. 5. 29..
 */
object OriginConnectionPool{
  val TRAFFIC_COUNT_INTERVAL = 0 //todo testing...  //300000
}

class OriginConnectionPool (config:Elem,
                            pBootstrap: Bootstrap,
                            pOriginWorkerGroup: NioEventLoopGroup,
                            pOriginTrafficCounter: TrafficCounter,
                            pClientTrafficCounter: TrafficCounter,
                            pOriginTrafficHandler: GlobalTrafficShapingHandler,
                            pClientTrafficHandler: GlobalTrafficShapingHandler,
                            pIdleChannels: mutable.SynchronizedQueue[Channel]) {

  def this(config:Elem)=this(config,null,null,null,null,null,null,null)

  val originHost = (config \ "@origin").toString()
  val host = (config \ "@host").toString()
  val port = {val str = (config \ "@port").toString(); if(str equals "") 80 else str.toInt}
  val originWorkerGroup = if(pOriginWorkerGroup!=null)pOriginWorkerGroup else new NioEventLoopGroup()
  val IdleChannels = if(pIdleChannels!=null) pIdleChannels else new mutable.SynchronizedQueue[Channel]

  val bootstrap: Bootstrap = if(pBootstrap!=null) pBootstrap else {
    val bootstrap = new Bootstrap()
    bootstrap.group(originWorkerGroup)
    bootstrap.channel(classOf[NioSocketChannel])
    bootstrap.option[java.lang.Boolean](ChannelOption.SO_KEEPALIVE, true)
    bootstrap.handler(new ChannelInitializer[SocketChannel] {
      override def initChannel(ch: SocketChannel) {
        ch.pipeline()
          .addFirst("HttpRequestHolder", new HttpRequestHolderHandler)
          .addFirst("contentDecoder", new HttpContentDecompressor())
          .addFirst("HttpEncoder", new HttpRequestEncoder)
          .addFirst("HttpDecoder", new HttpResponseDecoder())
          .addFirst("Logger",new LoggingHandler(LogLevel.DEBUG))
          .addFirst("HandlerAdder", new ChannelInboundHandlerAdapter {
          override def channelUnregistered(ctx: ChannelHandlerContext) = removeRuleHandler(ctx.channel().pipeline())
          override def exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) = cause match {
            case ex: java.io.IOException => println("IO Exception")
              ex.printStackTrace()
            case other => other.printStackTrace()
          }
        })


      }
    })
    bootstrap
  }
  val clientTrafficHandler = if(pClientTrafficHandler!=null) pClientTrafficHandler else new GlobalTrafficShapingHandler(originWorkerGroup,OriginConnectionPool.TRAFFIC_COUNT_INTERVAL)
  val originTrafficHandler = if(pOriginTrafficHandler!=null) pOriginTrafficHandler else new GlobalTrafficShapingHandler(originWorkerGroup,OriginConnectionPool.TRAFFIC_COUNT_INTERVAL)

  val clientTrafficCounter = if(pClientTrafficCounter!=null) pClientTrafficCounter else clientTrafficHandler.trafficCounter()
  val originTrafficCounter = if(pOriginTrafficCounter!=null) pOriginTrafficCounter else originTrafficHandler.trafficCounter()


  val cacheControlHandler = new CacheControlFilter(config)
  val proxyCacheHandler = new ProxyCacheFilter(config,this)
  val faviconHandler = new FaviconFilter(config)
  val autoProxyHandler = new AutoProxyFilter(host,config)

  def restartConnectionPool(config:Elem)={
    new OriginConnectionPool(config){
      override val clientTrafficHandler = OriginConnectionPool.this.clientTrafficHandler
      override val originTrafficHandler = OriginConnectionPool.this.originTrafficHandler
      override val clientTrafficCounter = OriginConnectionPool.this.clientTrafficCounter
      override val originTrafficCounter = OriginConnectionPool.this.originTrafficCounter
    }
  }

  def proxyChannel(fn: (Channel) => Unit) {
    if (IdleChannels.size > 0) {
      val ch = IdleChannels.dequeue()
      if (ch.isActive) {
        if (ch.pipeline().get("HttpRequestHolder").asInstanceOf[HttpRequestHolderHandler].isEmpty) fn(ch)
        else {
          proxyChannel(fn)
          IdleChannels.enqueue(ch)
        }
      }
      else proxyChannel(fn)
    }
    else {
      bootstrap.connect(originHost, port).addListener(new ChannelFutureListener {
        override def operationComplete(future: ChannelFuture): Unit = if (future.isSuccess) fn(future.channel())
      })
    }
  }

  def addRuleHandler(pipeline:ChannelPipeline){
    pipeline.addBefore("HttpRequestHolder","cache-control",cacheControlHandler())
    pipeline.addBefore("HttpRequestHolder","proxy-cache",proxyCacheHandler())
    pipeline.addBefore("HttpRequestHolder","add-favicon",faviconHandler())
    pipeline.addAfter("HttpRequestHolder","auto-proxy",autoProxyHandler())
    pipeline.addLast("traffic-counter",originTrafficHandler)
  }

  def removeRuleHandler(pipeline:ChannelPipeline){
    try {
      if(pipeline.get("add-favicon")!=null) pipeline.remove("add-favicon")
      if(pipeline.get("cache-control")!=null) pipeline.remove("cache-control")
      if(pipeline.get("proxy-cache")!=null) pipeline.remove("proxy-cache")
      if(pipeline.get("traffic-counter")!=null) pipeline.remove("traffic-counter")
    }
    catch {
      case ex:NoSuchElementException=> print("err?")
    }
  }

  def addClientHandler(pipeline:ChannelPipeline){
    pipeline.addLast("traffic-counter",clientTrafficHandler)
  }
  def removeClientHandler(pipeline:ChannelPipeline){
    pipeline.remove("traffic-counter")
  }
  val cacheMap = mutable.HashMap[String,CacheHolder]()
  def getCacheHolderActor(path:String)={
    cacheMap.getOrElse(path,{
      val newHolder = new CacheHolder(this,path)
      cacheMap+=path->newHolder
      newHolder
    })
  }

  def returnChannel(ch:Channel)=if(ch.isActive) {
    removeRuleHandler(ch.pipeline())
    //IdleChannels += ch
  }
}
