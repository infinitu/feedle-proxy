package com.feedle.proxy

import io.netty.channel.{Channel, ChannelHandlerContext, ChannelInboundHandlerAdapter}
import io.netty.handler.codec.http._
import io.netty.util.ReferenceCountUtil
import scala.collection.mutable

/**
 * Created by infinitu on 2014. 5. 30..
 */

object ConnectionStatus extends Enumeration{
  val WaitingRequest,WaitingChunk,WaitingConnect,Disconnected = Value
}

class ClientConnectionHandler(val proxyServer:ProxyServer) extends ChannelInboundHandlerAdapter {

  var status = ConnectionStatus.Disconnected
  var clientCtx:ChannelHandlerContext = null
  var channelToOrigin:Channel = null
  var originChannelPool:OriginConnectionPool = null
  var origin_keep_alive = true
  var client_keep_alive = true
  var handler_reg = false

  val connectionWaitingQueue = mutable.Queue.apply[HttpObject]()




  override def channelRegistered(ctx: ChannelHandlerContext) {
    println("registered")
    this.clientCtx=ctx
  }

  override def channelRead(ctx: ChannelHandlerContext, msg: scala.Any) = {
    this.clientCtx=ctx
    status match{
      case ConnectionStatus.Disconnected=>
        println("read")
        msg match {
          case req: HttpRequest =>

            //figure out host
            val host = req.headers().get("Host")
            println("connect to "+host)


            client_keep_alive = {
              val con = req.headers().get(HttpHeaders.Names.CONNECTION)
              if(con==null || "".equals(con))
                false
              else if(HttpHeaders.Values.KEEP_ALIVE equals con)
                true
              else
                false
            }


            val pool = proxyServer.originConnectionPoolMap.get(host)
            if(pool.isEmpty)
              ctx.close()
            else
            {
              connectionWaitingQueue.enqueue(ReferenceCountUtil.retain(req))
              this.status = ConnectionStatus.WaitingConnect

              originChannelPool=pool.get
              pool.get.proxyChannel(
              {(channel)=>
                this.channelToOrigin=channel
                pool.get.addRuleHandler(channel.pipeline())
                pool.get.addClientHandler(ctx.channel.pipeline())
                channel.pipeline().addLast("ClientConnectionHandler",new ChannelInboundHandlerAdapter{
                  override def channelRead(ctx: ChannelHandlerContext, msg: scala.Any){originRead(msg)}
                  override def channelInactive(inactiveCtx: ChannelHandlerContext): Unit = status=ConnectionStatus.Disconnected
                })
                handler_reg = true
                connectionWaitingQueue.dequeueAll{req=>
                  channel.write(req)
                  true
                  //ReferenceCountUtil.release(req)
                }
                channel.flush()
                this.status=ConnectionStatus.WaitingRequest
              }
              )
            }

          case _ => ctx.close()
            println("closes 2")
        }
      case ConnectionStatus.WaitingConnect=>
        connectionWaitingQueue.enqueue(ReferenceCountUtil.retain(msg).asInstanceOf[HttpObject])
      case ConnectionStatus.WaitingRequest=>
        //todo filtering!!
        channelToOrigin.writeAndFlush(msg)
        ReferenceCountUtil.release(msg)
      case ConnectionStatus.WaitingChunk=>
        channelToOrigin.writeAndFlush(msg)
        ReferenceCountUtil.release(msg)

    }
  }

  def originRead(msg:scala.Any){
    msg match{
      case response:HttpResponse=>
        println(response.toString)
        val con = response.headers().get("Connection")
        if(con!=null && con.equals("keep-alive")) origin_keep_alive = true
      case lastContent:LastHttpContent=>
        println("last" + lastContent.toString)

      case content:HttpContent=>
        println(content.toString)
    }

    clientCtx.channel().writeAndFlush(msg)
    if(msg.isInstanceOf[LastHttpContent] && !client_keep_alive) clientCtx.close()
  }

  override def channelInactive(ctx: ChannelHandlerContext)={
    if(channelToOrigin!=null && origin_keep_alive)
    {
      try{channelToOrigin.pipeline().remove("ClientConnectionHandler")}
      catch {
        case e:NoSuchElementException=> //ignore
      }
      originChannelPool.removeClientHandler(ctx.channel().pipeline())
      originChannelPool.returnChannel(channelToOrigin)
    }
    connectionWaitingQueue.dequeueAll(x=>ReferenceCountUtil.release(x))
    println("inactive")
  }

  override def exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable)=cause match{
    case ex:java.io.IOException=>
      ex.printStackTrace()
      println("ioexception")
      ctx.close
    case other=>other.printStackTrace()
  }
}
