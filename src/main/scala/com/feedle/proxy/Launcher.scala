package com.feedle.proxy

import com.feedle.proxy.controller.CommandServer

/**
 * Created by infinitu on 2014. 5. 29..
 */
object Launcher extends App{
  override def main(args: Array[String]) {
    val proxyServer = new ProxyServer(8088)
    val commandServer = new CommandServer(proxyServer,9458)
    //commandServer.run()
    proxyServer.run()
  }

}
