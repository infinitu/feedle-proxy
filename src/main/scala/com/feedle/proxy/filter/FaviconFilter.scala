package com.feedle.proxy.filter

import io.netty.buffer.Unpooled
import io.netty.channel.{ChannelDuplexHandler, ChannelHandlerContext, ChannelPromise}
import io.netty.handler.codec.base64.Base64
import io.netty.handler.codec.http._
import io.netty.util.ReferenceCountUtil

import scala.xml.Elem


/**
 * Created by infinitu on 2014. 6. 1..
 */
class FaviconFilter(val config:Elem){
  val globalSetting = (config \ "GlobalRule").filter(x=>(x \ "@name").toString equals "favicon")
  val globalRule = if(globalSetting.isEmpty) None
  else Some({
    val req = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,HttpResponseStatus.OK,
      Base64.decode(Unpooled.wrappedBuffer((globalSetting(0) \ "@image").toString().getBytes)))
    req.headers().set("ContentType","image/png")
    req
  })
  def apply()={
    new ChannelDuplexHandler{

      var holder:HttpRequestHolderHandler=null
      var passThrough:Boolean = false
      var pathHold = ""
      override def handlerAdded(ctx: ChannelHandlerContext){holder=ctx.channel().pipeline().get("HttpRequestHolder").asInstanceOf[HttpRequestHolderHandler]}

      override def handlerRemoved(ctx: ChannelHandlerContext){holder=null}

      override def write(ctx: ChannelHandlerContext, msg: scala.Any, promise: ChannelPromise){
        if(globalRule.isEmpty)
          ctx.write(msg)
        else
          msg match {
            case req:HttpRequest=>
              val path = req.getUri
              if("/favicon.ico" equals path)
              {
                pathHold=path
                if (req.isInstanceOf[LastHttpContent] && holder.size == 1) {
                  val nctx = ctx.pipeline().firstContext()
                  nctx.fireChannelRead(ReferenceCountUtil.retain(globalRule.get))

                  passThrough=false
                }
                else {
                  passThrough = true
                }
                ReferenceCountUtil.release(msg)
              }
              else
                ctx.write(msg)
            case _:LastHttpContent=>
              if(passThrough){
                if (holder.size == 1) {
                  val nctx = ctx.pipeline().firstContext()
                  nctx.fireChannelRead(ReferenceCountUtil.retain(globalRule.get))
                }
                ReferenceCountUtil.release(msg)
                passThrough=false
              }
              else{
                ctx.write(msg)
              }
            case _=>
              if(!passThrough){
                ctx.write(msg)
              }
              else
                ReferenceCountUtil.release(msg)
          }
      }

      override def channelRead(ctx: ChannelHandlerContext, msg: scala.Any){
        if(globalRule.isEmpty)
          ctx.fireChannelRead(msg)
        else
          msg match {
            case _:LastHttpContent=>

              if(holder!=null && holder.size == 2) {
                val path = holder.httpRequestQueue.get(1).get.getUri
                if ("/favicon.ico" equals path) {
                  ctx.fireChannelRead(msg)
                  val nctx = ctx.pipeline().firstContext()
                  nctx.fireChannelRead(ReferenceCountUtil.retain(globalRule.get))
                  ReferenceCountUtil.release(msg)
                }
                else
                  ctx.fireChannelRead(msg)
              }
              else
                ctx.fireChannelRead(msg)
            case _=>
              ctx.fireChannelRead(msg)
          }
      }
    }
  }
}
