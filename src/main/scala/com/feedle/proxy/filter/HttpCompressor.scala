package com.feedle.proxy.filter

import io.netty.handler.codec.http.{HttpHeaders, HttpResponse, HttpContentCompressor}
import scala.xml.{NodeSeq, Elem}
import io.netty.handler.codec.http.HttpContentEncoder.Result
import io.netty.channel.{ChannelPromise, ChannelHandlerContext}

/**
 * Created by infinitu on 2014. 6. 10..
 */
class HttpCompressor() extends {
//  val globalSetting = (config \ "GlobalRule").filter(x => (x \ "@name").toString equals "accept-encoding")
//  val rule = if (globalSetting.isEmpty) 0 else (globalSetting(0) \ "@compress-level").toString().toInt
  def apply()={
   new HttpContentCompressor(6){
     override def write(ctx: ChannelHandlerContext, msg: scala.Any, promise: ChannelPromise): Unit = super.write(ctx, msg, promise)

     override def channelRead(ctx: ChannelHandlerContext, msg: scala.Any): Unit = super.channelRead(ctx, msg)

     override def beginEncode(headers: HttpResponse, acceptEncoding: String): Result = {
       val alreadyEnc = headers.headers().get(HttpHeaders.Names.CONTENT_ENCODING)
       if(alreadyEnc==null)
         super.beginEncode(headers, acceptEncoding)
       else if(alreadyEnc.equalsIgnoreCase("gzip"))
         null
       else if (alreadyEnc.equalsIgnoreCase("deflate"))
         null
       else
         super.beginEncode(headers, acceptEncoding)
     }
   }
  }
}
