package com.feedle.proxy.filter

import scala.xml.Elem
import com.feedle.proxy.OriginConnectionPool
import io.netty.channel._
import scala.collection.mutable
import io.netty.handler.codec.http._
import io.netty.util.ReferenceCountUtil

/**
 * Created by infinitu on 2014. 6. 6..
 */
class ProxyCacheFilter(val cacheableList:Seq[String],originPool:OriginConnectionPool){

  def this(config:Elem, originPool:OriginConnectionPool)=
    this(
      for (
        i<-config \ "RuleGroup"
        if (i \ "@proxyCacheable").toString() equals "true"
      ) yield (i \ "@path").toString()
      ,
      originPool)

  def apply()={
    new ChannelDuplexHandler{

      var holder:HttpRequestHolderHandler=null
      var passThrough:Boolean = false
      var pathHold = ""
      override def handlerAdded(ctx: ChannelHandlerContext){holder=ctx.channel().pipeline().get("HttpRequestHolder").asInstanceOf[HttpRequestHolderHandler]}

      override def handlerRemoved(ctx: ChannelHandlerContext){holder=null}

      override def write(ctx: ChannelHandlerContext, msg: scala.Any, promise: ChannelPromise){
        msg match {
          case req:HttpRequest=>
            val path = req.getUri
            if(cacheableList.contains(path))
            {
              pathHold=path
              if (req.isInstanceOf[LastHttpContent] && holder.size == 1) {
                originPool.getCacheHolderActor(pathHold) ! ctx.pipeline().firstContext()
                passThrough=false
              }
              else {
                originPool.getCacheHolderActor(pathHold)
                passThrough = true
              }
            }
            else
              ctx.write(req)
          case last:LastHttpContent=>
            if(passThrough){
              if (holder.size == 1) {
                originPool.getCacheHolderActor(pathHold) ! ctx.pipeline().firstContext()
              }
              passThrough=false
            }
            else{
              ctx.write(last)
            }
          case other=>
            if(!passThrough){
              ctx.write(other)
            }
        }
      }

      override def channelRead(ctx: ChannelHandlerContext, msg: scala.Any){
        msg match {
          case last:LastHttpContent=>

            if(holder!=null && holder.size == 2) {
              val path = holder.httpRequestQueue.get(1).get.getUri
              if (cacheableList.contains(path)) {
                ctx.fireChannelRead(last)
                originPool.getCacheHolderActor(path) ! ctx
                ReferenceCountUtil.release(last)
              }
              else
                ctx.fireChannelRead(last)
            }
            else
              ctx.fireChannelRead(last)
          case other=>
            ctx.fireChannelRead(other)
        }
      }
    }
  }
}



class CacheHolder(originPool:OriginConnectionPool,path:String){

  val waitingConnection = new mutable.ArrayBuffer[ChannelHandlerContext] with mutable.SynchronizedBuffer[ChannelHandlerContext]
  val cacheData = new mutable.ArrayBuffer[Any] with mutable.SynchronizedBuffer[Any]
  var pending = true
  val request = new DefaultHttpRequest(HttpVersion.HTTP_1_1,HttpMethod.GET,path)
  request.headers().add("host",originPool.host)
  request.headers().add("connection","keep-alive")


  originPool.proxyChannel{ch=>
    ch.writeAndFlush(request)
    ch.pipeline().addLast(new ChannelInboundHandlerAdapter{
      override def channelRead(ctx: ChannelHandlerContext, msg: scala.Any){
        cacheData+=ReferenceCountUtil.retain(msg)
        waitingConnection.foreach{x=>
          msg match{
            case cont:HttpContent=>x.fireChannelRead(ReferenceCountUtil.retain(cont.duplicate()))
            case _ =>x.fireChannelRead(ReferenceCountUtil.retain(msg))
          }

        }
        msg match{
          case last:LastHttpContent=>
            pending=false
            waitingConnection.clear()
            originPool.returnChannel(ch)
          case _=>
        }
      }
    })
  }

  def !(ctx:ChannelHandlerContext)={
    cacheData.foreach{msg=>
      msg match{
        case cont:HttpContent=>ctx.fireChannelRead(ReferenceCountUtil.retain(cont.duplicate()))
        case _ =>ctx.fireChannelRead(ReferenceCountUtil.retain(msg))
      }
    }
    if(pending) waitingConnection+=ctx
  }
}

