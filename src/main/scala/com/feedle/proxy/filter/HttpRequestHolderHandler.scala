package com.feedle.proxy.filter

import scala.collection.mutable
import io.netty.channel.{ChannelPromise, ChannelHandlerContext, ChannelDuplexHandler}
import io.netty.handler.codec.http.{LastHttpContent, HttpRequest}
import io.netty.util.ReferenceCountUtil

/**
 * Created by infinitu on 2014. 6. 11..
 */

class HttpRequestHolderHandler extends ChannelDuplexHandler{

  val httpRequestQueue = new mutable.SynchronizedQueue[HttpRequest]

  override def write(ctx: ChannelHandlerContext, msg: scala.Any, promise: ChannelPromise)={msg match{
    case req:HttpRequest=> httpRequestQueue enqueue ReferenceCountUtil.retain(req)
    case _=>
  };ctx.write(msg)}

  override def channelRead(ctx: ChannelHandlerContext, msg: scala.Any){
    if(msg.isInstanceOf[LastHttpContent])
      ReferenceCountUtil.release(httpRequestQueue.dequeue())
    ctx.fireChannelRead(msg)
  }

  def front=httpRequestQueue.front

  def size=httpRequestQueue.size
  def isEmpty=httpRequestQueue.isEmpty
}
