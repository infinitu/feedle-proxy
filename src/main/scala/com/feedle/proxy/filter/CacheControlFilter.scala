package com.feedle.proxy.filter

import io.netty.channel.{ChannelHandlerContext, ChannelInboundHandlerAdapter}
import scala.xml.Elem
import io.netty.handler.codec.http.HttpResponse

/**
 * Created by infinitu on 2014. 6. 1..
 */
class CacheControlFilter(val config:Elem){
  val globalSetting = (config \ "GlobalRule").filter(x=>(x \ "@name").toString equals "cache-control")
  val globalRule = if(globalSetting.isEmpty) None else Some((globalSetting(0) \ "@maxage").toString().toInt)


  val localRules =
    (for (
      i<-config \ "RuleGroup";
      j<-i \ "Rule"
      if (j \ "@name").toString() equals "cache-control"
    ) yield (i \ "@path").toString -> (j \ "@maxage").toString().toInt).toMap

  def apply()= new ChannelInboundHandlerAdapter{

    var getNowRequest:HttpRequestHolderHandler=null

    override def handlerAdded(ctx: ChannelHandlerContext){
      getNowRequest=ctx.pipeline().get("HttpRequestHolder").asInstanceOf[HttpRequestHolderHandler]
    }

    override def handlerRemoved(ctx: ChannelHandlerContext){getNowRequest=null}

    override def channelRead(ctx: ChannelHandlerContext, msg: scala.Any)= {

      val nowReq = getNowRequest.front
      msg match {
        case response: HttpResponse =>
          val rule = if(localRules.isDefinedAt(nowReq.getUri)) localRules.get(nowReq.getUri) else globalRule
          if(rule.isDefined)
            response.headers().set("cache-control","max-age="+rule.get)
        case _ =>
      }
      ctx.fireChannelRead(msg)
    }
  }


}
