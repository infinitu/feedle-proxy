package com.feedle.proxy.filter

import java.lang.ref.Reference

import com.feedle.proxy.filter.autoproxy.{NotFoundNode, FoundNode, AbstractAutoProxyAutomataNode, ProxyAutomataFactory}
import io.netty.buffer.{Unpooled, ByteBuf}
import io.netty.channel.{ChannelPromise, ChannelDuplexHandler, ChannelHandlerContext, ChannelInboundHandlerAdapter}
import io.netty.handler.codec.http._
import io.netty.util.ReferenceCountUtil

import scala.collection.mutable
import scala.xml.Elem

/**
 * Created by infinitu on 2014. 6. 16..
 */
class AutoProxyFilter(val host:String, val config:Elem){
  val autoproxyRule =
    (for (
      i<-config \ "RuleGroup";
      j<-i \ "Rule"
      if (j \ "@name").toString() equals "auto-proxy"
    ) yield (i \ "@path").toString -> (j \ "@dest").toString()).toMap



  val automataHolder = ProxyAutomataFactory.createHolder(host)
  autoproxyRule.foreach(x=>automataHolder.addPath(x._1,x._2))

  def apply() = new ChannelDuplexHandler{
    var workingBuffer:ByteBuf=null
    var workingNode:AbstractAutoProxyAutomataNode=null

    var holder:AbstractAutoProxyAutomataNode = null

    val holderQ = new mutable.SynchronizedQueue[AbstractAutoProxyAutomataNode]()

    override def write(ctx: ChannelHandlerContext, msg: scala.Any, promise: ChannelPromise){
      msg match{
        case req:HttpRequest =>
          holderQ enqueue automataHolder.createAutomata(req.getUri)
          ctx.write(req)
        case other =>
          ctx.write(other)
      }
    }

    override def channelRead(ctx: ChannelHandlerContext, msg: scala.Any){
      msg match {
        case req : HttpResponse =>
          val hold = holderQ dequeue()
          val contentType=req.headers().get(HttpHeaders.Names.CONTENT_TYPE)
          if(contentType.contains("html") ||
              contentType.contains("stylesheet") ||
              contentType.contains("css"))
            holder = hold
          else
            holder = null
          ctx.fireChannelRead(req)
        case last : LastHttpContent if holder != null =>
          val buf =Unpooled.buffer(last.content.capacity)
          findTarget(last.content.duplicate(),buf)
          ReferenceCountUtil.release(last)
          if(workingBuffer!=null){
            upCapacity(buf,workingBuffer.readableBytes())
            buf.writeBytes(workingBuffer)
          }
          holder=null
          ctx.fireChannelRead(ReferenceCountUtil.retain(new DefaultLastHttpContent(buf)))
        case content : HttpContent if holder != null =>
          val buf =Unpooled.buffer(content.content.capacity)
          findTarget(content.content.duplicate(),buf)
          ReferenceCountUtil.release(content)
          ctx.fireChannelRead(ReferenceCountUtil.retain(new DefaultHttpContent(buf)))
        case other =>
          ctx.fireChannelRead(other)
      }
    }

    def findTarget(msg:ByteBuf,buf:ByteBuf ){
     if(msg.isReadable) {
       val startedIdx = msg.readerIndex()
       print(msg.getByte(startedIdx).toChar)
        if (workingBuffer != null) {
          workingNode.findPattern(msg, startedIdx) match {
            case found: FoundNode =>
              upCapacity(buf, found.foundUrl.length + 2)

              val quote = workingBuffer.readByte()
              buf.writeByte(quote)
              buf.writeBytes(found.foundUrl)
              buf.writeByte(quote)

              ReferenceCountUtil.release(workingBuffer)
              workingBuffer = null

              if (msg.isReadable)
                findTarget(msg, buf)

            case notFound: NotFoundNode =>
              upCapacity(buf, 2)
              buf.writeByte(workingBuffer.readByte() & 0xff)
              if (workingBuffer.isReadable) {
                val wworkingBuf = workingBuffer
                workingBuffer = null
                findTarget(wworkingBuf, buf)
                workingBuffer = wworkingBuf
                findTarget(msg, buf)
              }
              else {
                workingBuffer = null
                findTarget(msg, buf)
              }

            case doing =>
              val old = workingBuffer
              workingBuffer = workingBuffer.duplicate()
              upCapacity(workingBuffer, msg.writableBytes())
              workingBuffer.writeBytes(msg)
              ReferenceCountUtil.release(msg)
              ReferenceCountUtil.release(old)
              workingNode = doing
          }
        }
        else {
          holder.findPattern(msg, startedIdx) match {
            case found: FoundNode =>

              upCapacity(buf, found.foundUrl.length + 2)

              val quote = msg.getByte(startedIdx)
              buf.writeByte(quote)
              buf.writeBytes(found.foundUrl)
              buf.writeByte(quote)

              if(workingBuffer!=null) {
                ReferenceCountUtil.release(workingBuffer)
                workingBuffer = null
              }

              findTarget(msg,buf)

            case notFound: NotFoundNode =>

              upCapacity(buf, 2)
              buf.writeByte(msg.readByte() & 0xff)

              findTarget(msg,buf)

            case doing =>

              workingBuffer = ReferenceCountUtil.retain(msg)
              workingNode = doing

          }
        }
      }
    }

    def upCapacity(buf:ByteBuf,length:Int ){
      if(buf.writableBytes < length){
        buf.capacity(buf.capacity * 2)
        upCapacity(buf,length)
      }
    }

  }


}
